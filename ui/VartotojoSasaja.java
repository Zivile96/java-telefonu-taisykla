/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.ui;

import java.util.LinkedHashMap;
import java.util.Scanner;
import taisykla.main.*;

public class VartotojoSasaja {
    public int getIvesti(){
        Scanner skaitlys = new Scanner(System.in);
        if(skaitlys.hasNextInt()){
            return skaitlys.nextInt();
        }
    return 0;
    }
    public void useMeniu(Administratorius admin, Klientas klientas){
        System.out.println("Jeigu atejote atsiimti telefono, spauskite 0");
        System.out.println("Jeigu atejote duoti naujo telefono, spauskite 1");
        System.out.println("Jeigu norite iseiti, spauskite 2");
        int ivestis = getIvesti();
        if (ivestis == 0) returnTelefona(admin, klientas); 
        else if (ivestis == 1){
            inspectTelefonus(admin, klientas);
        }
        else if (ivestis == 2) System.exit(0);
        else{
            System.out.println("Atsiprasome, tokio pasirinkimo nera.");
            useMeniu(admin, klientas);
        }
    }
    public void returnTelefona(Administratorius admin, Klientas klientas){
        int ivestis;
        System.out.println("Iveskite telefono identifikacijos numeri");
        ivestis = getIvesti();
        if (admin.getTelefona(ivestis) != null){
            if (admin.getTelefona(ivestis).getArSutaisytas()){
                System.out.println("Jusu telefonas pataisytas!");
                if(!klientas.sumoketiUzRemonta(admin.getTelefona(ivestis).getTaisymoKaina(), 
                        klientas.getNuolaiduKortele())){
                    System.out.println("Atsiprasome, jusu saskaitoje nepakankamai pinigu sumoketi uz remonta. Telefono grazinti negalime.");
                    System.exit(0);
                }
                else{
                    if(!klientas.getNuolaiduKortele()){
                    System.out.println("Jus neturite nuolaidu korteles! "
                        + "Ar noretumete isigyti?");
                    System.out.println("Jei noretumete, spauskite 1");
                    if(getIvesti() == 1)
                        if(!klientas.addNuolaiduKortele()) System.out.println("Atsiprasome, jusu saskaitoje nepakanakamai pinigu.");
                        else{
                            System.out.println("Sveikiname sekmingai isigijus nuolaidu kortele!");
                        }
                    }
                    System.out.println("Sekmingai sumokejote uz remonta.");
                    admin.removeTelefona(ivestis);
                }    
            }
            else System.out.println("Atsiprasome, jusu telefonas dar taisomas");
            System.out.println("Jeigu norite testi, spauskite 0");
            System.out.println("Jeigu norite iseiti, spauskite 1");
            ivestis = getIvesti();
            if (ivestis == 0) useMeniu(admin, klientas);
            else if (ivestis == 1) System.exit(0);
            else{
                System.out.println("Atsiprasome, tokio pasirinkimo nera.");
                useMeniu(admin, klientas);
            }    
        }    
        else System.out.println("Atsiprasome, telefono su tokiu identifikacijos numeriu neturime.");
        System.out.println("Jeigu norite bandyti is naujo, spauskite 0");
        System.out.println("Jeigu norite iseiti, spauskite 1");
        ivestis = getIvesti();
        if (ivestis == 0) returnTelefona(admin, klientas);
        else if (ivestis == 1) System.exit(0);
        else{
            System.out.println("Atsiprasome, tokio pasirinkimo nera.");
            useMeniu(admin, klientas);
        }
    }
    public void inspectTelefonus(Administratorius admin, Klientas klientas){
        int ivestis;
        for(int i = 0; i < klientas.getKiekTelefonu(); i++){
            System.out.println("Telefono modelis: " +  klientas.getTelefona(i).getModeli());
            if (klientas.getTelefona(i).getArSutaisytas()){
                System.out.println("Sis telefonas neturi gedimu.");
                continue;
            }
            System.out.println("Sugedusi detale:" + klientas.getTelefona(i).getSugedusiaDetale());
            System.out.println("Taisymo kaina:" + klientas.getTelefona(i).getTaisymoKaina());
            System.out.println("Jei norite taisyti si telefona, spauskite 1");
            ivestis = getIvesti();
            if (ivestis == 1){
                System.out.println("Puiku! Telefonas bus sutaisytas savaites begyje.");
                taisytiTelefona(admin, klientas.getTelefona(i).getIdentifikacNr());
            }
            if (i < klientas.getKiekTelefonu() - 1) System.out.println("Kitas jusu telefonas:");
        }
        System.out.println("Aciu, kad naudojates musu paslaugomis!");
    }
    public boolean taisytiTelefona(Administratorius admin, int key){
        for (int i = 0; i < admin.getKiekMeistru(); i++){
            if(admin.getMeistra(i).sutaisytiTelefona(admin, key)) return true;
        }
        return false;
    }
}