/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.main;

import java.util.ArrayList;

/**
 *
 * @author Zivile
 */
public class VipKlientas extends Klientas {
    private static double vipNuolKortelesProcentai = 20;
    private int lojalumoTaskai;
    
    public VipKlientas(String vardas, String pavarde, double piniguSuma, boolean nuolaiduKortele, int lojalumoTaskai){
    super(vardas, pavarde, piniguSuma, true);
    this.lojalumoTaskai = lojalumoTaskai;
    }
    public int getLojalTask(){
        return lojalumoTaskai;
    }
    @Override
    public boolean sumoketiUzRemonta(double kaina){
        if (super.getPiniguSuma() >= kaina*((100 - vipNuolKortelesProcentai)/100 - lojalumoTaskai*0.1)){
        super.setPiniguSuma(super.getPiniguSuma() - kaina*((100 - vipNuolKortelesProcentai)/100) - lojalumoTaskai*0.1);
        return true;
        }
        else return false;
    }
}
