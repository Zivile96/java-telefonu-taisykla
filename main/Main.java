package taisykla.main;
import taisykla.io.Rasymas;
import taisykla.io.Skaitymas;
import java.io.FileNotFoundException;
import java.io.IOException;
import taisykla.ui.VartotojoSasaja;

/**
 *
 * @author Zivile Kinduryte
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        Skaitymas read = new Skaitymas("C:\\Users\\Zivile\\Desktop\\Main\\failai\\klientas.txt", 
                "C:\\Users\\Zivile\\Desktop\\Main\\failai\\telefonai.txt", "C:\\Users\\Zivile\\Desktop\\Main\\failai\\admin.txt",
                "C:\\Users\\Zivile\\Desktop\\Main\\failai\\meistras.txt");
        Administratorius admin = read.getAdminDuomenis();
        Klientas klientas = read.getKlientoDuomenis(admin);
        read.readTelefonuDuomenis(admin);
        VartotojoSasaja ui = new VartotojoSasaja();
        read.getMeistroDuomenis(admin);

        Rasymas write = new Rasymas("C:\\Users\\Zivile\\Desktop\\Main\\failai\\klientas.txt", 
                "C:\\Users\\Zivile\\Desktop\\Main\\failai\\telefonai.txt", "C:\\Users\\Zivile\\Desktop\\Main\\failai\\admin.txt",
                "C:\\Users\\Zivile\\Desktop\\Main\\failai\\meistras.txt");
        
        ui.useMeniu(admin,klientas);
        write.rasytiTelefonus(admin);
        
        
        
        
                //ui.pasirinkimai()
        //admin.removeTelefona(123456);
        /*for(MobilusisTelefonas key: admin.getTelefonus().values()) {
            admin.removeTelefona(key);
            //System.out.println()
	}*/
        //write.rasytiKlienta(klientas);
        /*System.out.println("Darbuotojo testavimas: ");
        Meistras darbuotojas1 = new Meistras("Petras", "Petraitis", "vadybininkas", 600);
        System.out.println("Darbuotojo vardas: " + darbuotojas1.gautiVarda());
        System.out.println("Darbuotojo pavarde: " + darbuotojas1.gautiPavarde());
        System.out.println("Darbuotojo pareigos: " + darbuotojas1.gautiPareigas());
        System.out.println("Darbuotojo alga: " + darbuotojas1.gautiAlga());
        Meistras darbuotojas2 = new Meistras();
        System.out.println("Darbuotojo vardas: " + darbuotojas2.gautiVarda());
        System.out.println("Darbuotojo pavarde: " + darbuotojas2.gautiPavarde());
        System.out.println("Darbuotojo pareigos: " + darbuotojas2.gautiPareigas());
        System.out.println("Darbuotojo alga: " + darbuotojas2.gautiAlga());
        
        System.out.println("Kliento testavimas: ");
        Klientas klientas1 = new Klientas("Ona", "Onyte", 60, false);
        System.out.println("Kliento vardas: " + klientas1.gautiVarda());
        System.out.println("Kliento pavarde: " + klientas1.gautiPavarde());
        System.out.println("Kliento pinigu suma: " + klientas1.gautiPiniguSuma());
        klientas1.pirktiNuolaiduKortele();
        System.out.println("Kliento pinigu suma: " + klientas1.gautiPiniguSuma());
        System.out.print("Ar turi nuolaidu kortele?: ");
        if (klientas1.arYraNuolaiduKortele() == true)
            System.out.println("Taip");
        else
            System.out.println("Ne");
        klientas1.sumoketiUzRemonta(20);
        System.out.println("Kliento pinigu suma: " + klientas1.gautiPiniguSuma());
        
        System.out.println("Mobiliojo telefono testavimas: ");
        MobilusisTelefonas telefonas1 = new MobilusisTelefonas();
        System.out.println("Modelis: " + telefonas1.gautiModeli());
        System.out.println("Sugedusi detale: " + telefonas1.gautiSugedusiaDetale());
        System.out.println("Dienu skaicius remontui: " + telefonas1.gautiRemontoTrukme());
        System.out.println("Ar sutaisytas: " + telefonas1.arSutaisytas());
        MobilusisTelefonas telefonas2 = new MobilusisTelefonas("Samsung Galaxy S3", "Kamera", 20, false);
        telefonas2.telefonasSutaisytas();
        System.out.println("Ar sutaisytas: " + telefonas2.arSutaisytas());*/
        
    }
    
}