package taisykla.main;

/**
 *
 * @author Zivile Kinduryte
 */
public class MobilusisTelefonas implements Taisomas {
    private final String modelis;
    private String sugedusiDetale;
    private boolean arSutaisytas;
    private final int identifikacNr;
    private double kainaTaisymo;
    
    public MobilusisTelefonas(String modelis, String sugedusiDetale, boolean arSutaisytas, int identifikacNr, double kainaTaisymo){
        this(modelis, sugedusiDetale, identifikacNr);
        this.arSutaisytas = arSutaisytas;
        this.kainaTaisymo = kainaTaisymo;         
    }
    public MobilusisTelefonas(String modelis, String sugedusiDetale, int identifikacNr){
        this.modelis = modelis;
        this.sugedusiDetale = sugedusiDetale;
        this.identifikacNr = identifikacNr;
    }
    public MobilusisTelefonas(){
        this(null, null, false, -1, -1);
    }
    public String getModeli(){
        return modelis;
    }
    public String getSugedusiaDetale(){
        return sugedusiDetale;
    }
    public boolean getArSutaisytas(){
        return arSutaisytas;
    }
    @Override
    public int getIdentifikacNr(){
        return identifikacNr;
    }
    @Override
    public double getTaisymoKaina(){
        return kainaTaisymo;
    }
    @Override
    public boolean sutaisyti(){
        arSutaisytas = true;
        sugedusiDetale = null;
        return true;
    }
    @Override
    public String toString(){
        return modelis + "-" + sugedusiDetale + "-" + arSutaisytas + "-" + identifikacNr + "-" + kainaTaisymo;
    }
}