/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.main;

/**
 *
 * @author Zivile
 */
public class Virsininkas extends Administratorius {
    
    private static double standartAlgosKelimas = 50;
    
    public Virsininkas(String vardas, String pavarde, int alga) {
        super(vardas, pavarde, alga);
    }
    public double getAlgosKelima(){
        return standartAlgosKelimas;
    }
    public void keltiAdminAlga(Administratorius admin){
        admin.setAlga(admin.getAlga() + standartAlgosKelimas);
    }
    public void keltiMeistroAlga(Meistras meistras){
        meistras.setAlga(meistras.getAlga() + standartAlgosKelimas);
    }
    
}
