/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.main;

/**
 *
 * @author Zivile
 */
public abstract class Zmogus {
    private final String vardas;
    private String pavarde;
    private boolean nuolaiduKortele;
    
    public Zmogus(String vardas, String pavarde){
        this.vardas = vardas;
        this.pavarde = pavarde;
    }
    public Zmogus(String vardas, String pavarde, boolean nuolaiduKortele){
        this(vardas, pavarde);
        this.nuolaiduKortele = nuolaiduKortele;
    }
    
    abstract boolean addNuolaiduKortele();
    
    public String getVarda(){
        return vardas;
    }
    public String getPavarde(){
        return pavarde;
    }
    public boolean getNuolaiduKortele(){
        return nuolaiduKortele;
    }
    public void setNuolaiduKortele(boolean nuolaiduKortele){
        this.nuolaiduKortele = nuolaiduKortele;
    }
}
