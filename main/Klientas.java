package taisykla.main;

import java.util.ArrayList;

/**
 *
 * @author Zivile Kinduryte
 */
public class Klientas extends Zmogus {
    private double piniguSuma;
    private static double nuolaiduKortelesKaina = 1.2;
    private static double nuolKortelesProcentai = 5;
    private static ArrayList<MobilusisTelefonas> atnestiTelefonai;
    
    public Klientas(String vardas, String pavarde, double piniguSuma, boolean nuolaiduKortele){
        super(vardas, pavarde, nuolaiduKortele);
        this.piniguSuma = piniguSuma;
        atnestiTelefonai = new ArrayList<MobilusisTelefonas>();
    }
    public Klientas(String vardas, String pavarde, double piniguSuma){
       this(vardas, pavarde, piniguSuma, false);
    }
    public Klientas(){
        this(null, null, -1, false);
    }
    public double getPiniguSuma(){
        return piniguSuma;
    }
    public MobilusisTelefonas getTelefona(int ind){
        return atnestiTelefonai.get(ind);
    }
    public int getKiekTelefonu(){
        return atnestiTelefonai.size();
    }    
    public void setPiniguSuma(double piniguSuma){
        this.piniguSuma = piniguSuma;
    }
    public boolean addTelefona(MobilusisTelefonas telefonas){
        return atnestiTelefonai.add(telefonas);
    }
    @Override
    public boolean addNuolaiduKortele(){
        if (super.getNuolaiduKortele() == false && piniguSuma >= nuolaiduKortelesKaina){
            piniguSuma -= nuolaiduKortelesKaina;
            super.setNuolaiduKortele(true);
            return true;
        }
        return false;
    }
    public boolean sumoketiUzRemonta(double kaina){
        return sumoketiUzRemonta(kaina, false);
    }
    public boolean sumoketiUzRemonta(double kaina, boolean nuolaiduKortele){
        if (nuolaiduKortele){
            if (piniguSuma >= kaina*((100 - nuolKortelesProcentai)/100)){
            piniguSuma -= kaina*((100 - nuolKortelesProcentai)/100);
            return true;
            }
        }
        else if (piniguSuma >= kaina){
            piniguSuma -= kaina;
            return true;
        }
        return false;
    }
}