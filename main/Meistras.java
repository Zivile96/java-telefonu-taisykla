package taisykla.main;

/**
 *
 * @author Zivile Kinduryte
 */
public class Meistras extends Zmogus{
    private double alga;
    private static int maxTelSkaicius = 10;
    private int turimuTelSkaicius = 0;
    
    public Meistras(String vardas, String pavarde, double alga){
        super(vardas, pavarde);
        this.alga = alga;
    }
    public Meistras(){
        this(null, null, -1);
    }
    public double getAlga(){
        return alga;
    }
    public int getMaxTelefonuSkaicius(){
        return maxTelSkaicius;
    }
    public int getTurimuTelSk(){
        return turimuTelSkaicius;
    }
    public void setAlga(double alga){
        this.alga = alga;
    }
    @Override
    public boolean addNuolaiduKortele(){
        if (super.getNuolaiduKortele()) return false;
        super.setNuolaiduKortele(true);
        return true;
    }
    public void keistiMaxTelSkaiciu(int maksimalusTelefonuSkaicius){
        Meistras.maxTelSkaicius = maksimalusTelefonuSkaicius;
    }
    public boolean sutaisytiTelefona(Administratorius admin, int key){
        if (turimuTelSkaicius < maxTelSkaicius){
             admin.getTelefona(key).sutaisyti();
            return true;
        }
        else return false;
    }
    public void padidintiTelSkaiciu(){
        turimuTelSkaicius ++;
    }
}