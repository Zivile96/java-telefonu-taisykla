/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.main;

/**
 *
 * @author Zivile
 */
public interface Taisomas {
    
    String getSugedusiaDetale();
    
    int getIdentifikacNr();
    
    double getTaisymoKaina();
    
    boolean sutaisyti();
    
}
