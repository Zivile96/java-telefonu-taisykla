/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.main;

import java.util.LinkedHashMap;
import java.util.LinkedList;
/**
 *
 * @author Zivka
 */
public class Administratorius extends Zmogus {
    private double alga;
    private LinkedHashMap<Integer, MobilusisTelefonas> telefonuSarasas;
    private LinkedList<Meistras> meistruSarasas;
    
    public Administratorius(String vardas, String pavarde, double alga){
        super(vardas, pavarde);
        this.alga = alga;
        telefonuSarasas = new LinkedHashMap<Integer, MobilusisTelefonas>();
        meistruSarasas = new LinkedList<Meistras>();
    }
    public double getAlga(){
        return alga;
    }
    public void setAlga(double alga){
        this.alga = alga;
    }
    @Override
    public boolean addNuolaiduKortele(){
        if (super.getNuolaiduKortele()) return false;
        super.setNuolaiduKortele(true);
        return true;
    }
    public void addTelefona(MobilusisTelefonas telefonas){
        telefonuSarasas.put(telefonas.getIdentifikacNr(), telefonas);
    }
    public boolean removeTelefona(int key){
        if (telefonuSarasas.containsKey(key)){
            telefonuSarasas.remove(key);
            return true;
        }
        else return false;
    }
    public int getKiekTelefonu(){
        return telefonuSarasas.size();
    }
    public boolean addMeistra(Meistras meistras){
        return meistruSarasas.add(meistras);
    }
    public int getKiekMeistru(){
        return meistruSarasas.size();
    }
    public Meistras getMeistra(int kuris){
        return meistruSarasas.get(kuris);
    }
    public MobilusisTelefonas getTelefona(int key){
        if (telefonuSarasas.containsKey(key)) return telefonuSarasas.get(key);
        else return null;
    }
    public LinkedHashMap<Integer, MobilusisTelefonas> getTelefonus() {
    	return telefonuSarasas;
    }
    public void printTelefonus(){
        for (Integer key : telefonuSarasas.keySet()){
            System.out.println(telefonuSarasas.get(key));
        }
    }
}
