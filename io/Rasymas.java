/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.io;

import taisykla.main.Administratorius;
import taisykla.main.MobilusisTelefonas;
import taisykla.main.Klientas;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
/**
 *
 * @author Zivka
 */
public class Rasymas {
    private final String klientas;
    private final String telefonas;
    private final String admin;
    private final String meistras;
    
    public Rasymas(String klientas, String telefonas, String admin, String meistras){
        this.klientas = klientas;
        this.telefonas = telefonas;
        this.admin = admin;
        this.meistras = meistras;
    }
    public boolean rasytiKlienta(Klientas klientas) throws IOException{
        FileWriter writeKlientas = new FileWriter(new File(this.klientas));
        writeKlientas.append(klientas.getVarda() + "-" + klientas.getPavarde() + "-" + klientas.getPiniguSuma() 
                + "-" + klientas.getNuolaiduKortele());
        writeKlientas.close();
        return true;
    }
    public boolean rasytiTelefonus(Administratorius admin) throws IOException{
        FileWriter writeTelefonas = new FileWriter(new File(this.telefonas));
        MobilusisTelefonas telefonas;
        LinkedHashMap<Integer,MobilusisTelefonas> telefonuSarasas = admin.getTelefonus();
        for (Integer key : telefonuSarasas.keySet()){
            telefonas = telefonuSarasas.get(key);
            writeTelefonas.append(telefonas.getModeli() + "-" +  telefonas.getSugedusiaDetale() + "-" + telefonas.getArSutaisytas() +
                "-" + telefonas.getIdentifikacNr() + "-" + telefonas.getTaisymoKaina() + System.lineSeparator());
        }
        writeTelefonas.close();
        return true;
    }
}
