/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taisykla.io;

import taisykla.main.Meistras;
import taisykla.main.Administratorius;
import taisykla.main.MobilusisTelefonas;
import taisykla.main.Klientas;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import taisykla.main.*;
        
public class Skaitymas {
    private final String klientas;
    private final String telefonas;
    private final String admin;
    private final String meistras;
    
    public Skaitymas(String klientas, String telefonas, String admin, String meistras){
        this.klientas = klientas;
        this.telefonas = telefonas;
        this.admin = admin;
        this.meistras = meistras;
    }
    public Klientas getKlientoDuomenis(Administratorius admin) throws FileNotFoundException{
        Scanner skaitlys = new Scanner(new File(this.klientas));
        String eilute;
        String [] duomenys;
        eilute = skaitlys.nextLine();
        duomenys = eilute.split("-");
        Klientas klientas = new Klientas(duomenys[0], duomenys[1], Double.parseDouble(duomenys[2]), Boolean.parseBoolean(duomenys[3]));
        while(skaitlys.hasNextLine()){
            eilute = skaitlys.nextLine();
            duomenys = eilute.split("-");
            admin.addTelefona(new MobilusisTelefonas(duomenys[0], duomenys[1],
                Boolean.parseBoolean(duomenys[2]), Integer.parseInt(duomenys[3]), Double.parseDouble(duomenys[4])));
            klientas.addTelefona(new MobilusisTelefonas(duomenys[0], duomenys[1],
                Boolean.parseBoolean(duomenys[2]), Integer.parseInt(duomenys[3]), Double.parseDouble(duomenys[4])));
        }
        return klientas;
    }
    public boolean readTelefonuDuomenis(Administratorius admin) throws FileNotFoundException{
        Scanner skaitlys = new Scanner(new File(this.telefonas));
        String eilute;
        String [] duomenys;
        if(!skaitlys.hasNextLine()) return false;
        while(skaitlys.hasNextLine()){
            eilute = skaitlys.nextLine();
            duomenys = eilute.split("-");
            admin.addTelefona(new MobilusisTelefonas(duomenys[0], duomenys[1],
                Boolean.parseBoolean(duomenys[2]), Integer.parseInt(duomenys[3]), Double.parseDouble(duomenys[4])));
        }
        return true;
    }
    public Administratorius getAdminDuomenis() throws FileNotFoundException{
        Scanner skaitlys = new Scanner(new File(this.admin));
        String eilute;
        String [] duomenys;
        eilute = skaitlys.nextLine();
        duomenys = eilute.split("-");
        Administratorius admin = new Administratorius(duomenys[0], duomenys[1],
                Double.parseDouble(duomenys[2]));
        return admin;
    }
    public boolean getMeistroDuomenis(Administratorius admin) throws FileNotFoundException{
        Scanner skaitlys = new Scanner(new File(this.meistras));
        String eilute;
        String [] duomenys;
        while (skaitlys.hasNextLine()){
            eilute = skaitlys.nextLine();
            duomenys = eilute.split("-");
            admin.addMeistra(new Meistras(duomenys[0], duomenys[1],
                Double.parseDouble(duomenys[2])));
        }
        return true;
    }
}
